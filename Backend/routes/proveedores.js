'use strict'

var express = require('express');
var ProveedorController = require('../controllers/proveedores');
var router = express.Router();

router.post('/save', ProveedorController.save);
router.get('/getProveedor', ProveedorController.getProveedores);
//router.put('/proveedor/:id', ProveedorController.update);
router.delete('/proveedor/:id', ProveedorController.delete);

module.exports = router; 