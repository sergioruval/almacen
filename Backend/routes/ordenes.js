'use strict'

var express = require('express');
var OrdenController = require('../controllers/ordenes');
var router = express.Router();

router.post('/save', OrdenController.save);
router.get('/getOrden', OrdenController.getOrdenes);
//router.put('/articulo/:id', SalidasController.update);
//router.delete('/articulo/:id',ArticuloController.delete);

module.exports = router; 