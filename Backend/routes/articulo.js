'use strict'

var express = require('express');
var ArticuloController = require('../controllers/articulo');
var router = express.Router();

router.post('/save', ArticuloController.save);
router.get('/articulos', ArticuloController.getArticulos);
router.get('/articulo/:id',ArticuloController.getArticulo);
router.put('/articulo/:id', ArticuloController.update);
router.delete('/articulo/:id',ArticuloController.delete);

module.exports = router; 