'use strict'

var express = require('express');
var RecepcionController = require('../controllers/recepcion');
var router = express.Router();

router.post('/save', RecepcionController.save);
router.get('/getRecepciones', RecepcionController.getRecepcion);
//router.put('/almacen/:id', AlmacenController.update);
router.delete('/recepcion/:id', RecepcionController.delete);

module.exports = router; 