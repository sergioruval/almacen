'use strict'

var express = require('express');
var SalidasController = require('../controllers/salidas');
var router = express.Router();

router.post('/save', SalidasController.save);
router.get('/getSalidas', SalidasController.getSalidas);
//router.put('/articulo/:id', SalidasController.update);
//router.delete('/articulo/:id',ArticuloController.delete);

module.exports = router; 