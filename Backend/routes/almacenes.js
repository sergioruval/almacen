'use strict'

var express = require('express');
var AlmacenController = require('../controllers/almacenes');
var router = express.Router();

router.post('/save', AlmacenController.save);
router.get('/getAlmacenes', AlmacenController.getAlmacenes);
//router.put('/almacen/:id', AlmacenController.update);
router.delete('/almacen/:id', AlmacenController.delete);

module.exports = router; 