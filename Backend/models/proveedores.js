'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProveedoresSchema = Schema({
    nombre: String
});

module.exports = mongoose.model('Proveedores', ProveedoresSchema);