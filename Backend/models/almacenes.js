'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlmacenesSchema = Schema({
    nombre: String
});

module.exports = mongoose.model('Almacenes', AlmacenesSchema);