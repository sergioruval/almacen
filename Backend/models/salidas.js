'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SalidasSchema = Schema({
    articulo_name: String,
    articulos_cant: Number,
    almacen: String
});

module.exports = mongoose.model('Salidas', SalidasSchema);