'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArticuloSchema = Schema({
    nombre: String, 
    precio: String,
    topeMinimo: Number,
    topeMaximo: Number
});

module.exports = mongoose.model('Articulo', ArticuloSchema);