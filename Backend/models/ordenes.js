'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrdenSchema = Schema({
    articulo_name: String,
    articulos_cant: Number,
    proveedor: String
});

module.exports = mongoose.model('Ordenes', OrdenSchema);