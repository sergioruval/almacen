'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RecepcionSchema = Schema({
    tipoRecepcion: String,
    articulos: String
});

module.exports = mongoose.model('Recepcion', RecepcionSchema);