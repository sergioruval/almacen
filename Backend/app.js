'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var rutas_articulos = require('./routes/articulo');
var rutas_almacen = require('./routes/almacenes');
var rutas_proveedor = require('./routes/proveedores');
var rutas_recepcion = require('./routes/recepcion');
var rutas_salidas = require('./routes/salidas');
var rutas_ordenes = require('./routes/ordenes');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});

app.use('/articulos',rutas_articulos);
app.use('/almacenes', rutas_almacen);
app.use('/proveedores', rutas_proveedor);
app.use('/enrecepcion', rutas_recepcion);
app.use('/salidas', rutas_salidas);
app.use('/ordenes', rutas_ordenes);

module.exports = app;