'use strict'
var validator = require('validator');
var Almacenes = require('../models/almacenes');

var controller = {
    save: (req, res) => {
        var params = req.body;

        try {
            var validar_nombre = !validator.isEmpty(params.nombre);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!'
            });
        }

        if (validar_nombre) {
            var almacen = new Almacenes();

            almacen.nombre = params.nombre;

            almacen.save((err, AlmacenSaved) => {
                if (err || !AlmacenSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'El almacen no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    almacen: AlmacenSaved
                });
            });
        }
    },
    getAlmacenes: (req, res) => {
        var query = Almacenes.find({});

        query.sort('-_id').exec((err, almacenes) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los almacenes'
                });
            }

            if (!almacenes) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay almacenes para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                almacenes
            });
        });
    },
    delete: (req, res) => {
        var almacenId = req.params.id;
        Almacenes.findOneAndDelete({ _id: almacenId }, (err, almacenRemoved) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar'
                });
            }

            if (!almacenRemoved) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado el almacen, probablemente no exista.'
                });
            }

            return res.status(200).send({
                status: 'success',
                article: almacenRemoved
            });
        });

    }
}

module.exports = controller;
