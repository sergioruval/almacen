'use strict'
var validator = require('validator');
var Recepcion = require('../models/recepcion');

var controller = {
    save: (req, res) => {
        var params = req.body;

        try {
            var validar_tipo = !validator.isEmpty(params.tipoRecepcion);
            var validar_articulos = !validator.isEmpty(params.articulos);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!'
            });
        }

        if (validar_tipo && validar_articulos) {
            var recepcion = new Recepcion();

            recepcion.articulos = params.articulos;
            recepcion.tipoRecepcion = params.tipoRecepcion;

            recepcion.save((err, recepcionSaved) => {
                if (err || !recepcionSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'La recepcion no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    recepcion: recepcionSaved
                });
            });
        }
    },
    getRecepcion: (req, res) => {
        var query = Recepcion.find({});

        query.sort('-_id').exec((err, recepciones) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver las recepciones'
                });
            }

            if (!recepciones) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay recepciones para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                recepciones
            });
        });
    },
    delete: (req, res) => {
        var recepcionId = req.params.id;
        Recepcion.findOneAndDelete({ _id: recepcionId }, (err, recepcionRemoved) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar'
                });
            }

            if (!recepcionRemoved) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado la recepción, probablemente no exista.'
                });
            }

            return res.status(200).send({
                status: 'success',
                recepcion: recepcionRemoved
            });
        });

    }
}

module.exports = controller;