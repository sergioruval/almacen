'use strict'
var validator = require('validator');

var Articulo = require('../models/articulo');
var Salidas = require('../models/salidas');
var controller = {  
    save: (req, res) => {
        var params = req.body;

            var salidas = new Salidas();

            salidas.articulo_name = params.articulo_name;
            salidas.articulos_cant = params.articulos_cant;
            salidas.almacen = params.almacen;
           

            salidas.save((err, SalidasSaved) => {
                if (err || !SalidasSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'La salida no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    salida: SalidasSaved
                });
            });
        
    },
    getSalidas: (req, res) => {
        var query = Salidas.find({});

        query.sort('-_id').exec((err, salidas) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver la salida'
                });
            }

            if (!salidas) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay salidas para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                salidas
            });
        });
    }
}

module.exports = controller;