'use strict'

var Ordenes = require('../models/ordenes');
var controller = {  
    save: (req, res) => {
        var params = req.body;

            var ordenes = new Ordenes();

            ordenes.articulo_name = params.articulo_name;
            ordenes.articulos_cant = params.articulos_cant;
            ordenes.proveedor = params.proveedor;
           

            ordenes.save((err, OrdenSaved) => {
                if (err || !OrdenSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'La orden no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    orden: OrdenSaved
                });
            });
        
    },
    getOrdenes: (req, res) => {
        var query = Ordenes.find({});

        query.sort('-_id').exec((err, ordenes) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los ordenes'
                });
            }

            if (!ordenes) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay ordenes para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                ordenes
            });
        });
    }
}

module.exports = controller;