'use strict'
var validator = require('validator');

var Articulo = require('../models/articulo');

var controller = {
    save: (req, res) => {
        var params = req.body;

        try {
            var validar_precio = !validator.isEmpty(params.precio);
            var validar_nombre = !validator.isEmpty(params.nombre);
            var validar_topemin = !validator.isEmpty(params.topeMinimo);
            var validar_topemax = !validator.isEmpty(params.topeMaximo);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!'
            });
        }

        if (validar_precio && validar_nombre && validar_topemin && validar_topemax) {
            var articulo = new Articulo();

            articulo.nombre = params.nombre;
            articulo.precio = parseFloat(params.precio);
            articulo.topeMinimo = params.topeMinimo;
            articulo.topeMaximo = params.topeMaximo;

            articulo.save((err, ArticuloSaved) => {
                if (err || !ArticuloSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'El articulo no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    articulo: ArticuloSaved
                });
            });
        }
    },
    getArticulos: (req, res) => {
        var query = Articulo.find({});

        query.sort('-_id').exec((err, articulos) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los articulos'
                });
            }

            if (!articulos) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay articulos para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                articulos
            });
        });
    },
    getArticulo: (req, res) => {
        var articuloId = req.params.id;

        if (!articuloId || articuloId == null) {
            return res.status(404).send({
                status: 'error',
                message: 'No hay articulos para mostrar '
            });
        }

        Articulo.findById(articuloId, (err, articulo) => {
            if (err || !articulo) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existe el articulo '
                });
            }

            return res.status(200).send({
                status: 'success',
                articulo
            });
        });

    },
    update: (req, res) => {
        var articuloId = req.params.id;
        var params = req.body;

        Articulo.findOneAndUpdate({ _id: articuloId }, params, { new: true }, (err, articuloUpdated) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al actualizar.'
                });
            }

            if (!articuloUpdated) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existe el articulo'
                });
            }

            return res.status(200).send({
                status: 'success',
                articulo: articuloUpdated
            });
        });


    },
    delete: (req, res) => {
        var articuloId = req.params.id;

        Articulo.findOneAndDelete({ _id: articuloId }, (err, articuloRemoved) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar'
                });
            }

            if (!articuloRemoved) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado el articulo, probablemente no exista.'
                });
            }

            return res.status(200).send({
                status: 'success',
                article: articuloRemoved
            });
        });

    }
}

module.exports = controller;
