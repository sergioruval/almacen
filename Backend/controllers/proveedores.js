'use strict'
var validator = require('validator');
var Proveedores = require('../models/proveedores');

var controller = {
    save: (req, res) => {
        var params = req.body;

        try {
            var validar_nombre = !validator.isEmpty(params.nombre);

        } catch (err) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!'
            });
        }

        if (validar_nombre) {
            var proveedores = new Proveedores();

            proveedores.nombre = params.nombre;

            proveedores.save((err, ProveedorSaved) => {
                if (err || !ProveedorSaved) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'El proveedor no se ha guardado.'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    proveedor: ProveedorSaved
                });
            });
        }
    },
    getProveedores: (req, res) => {
        var query = Proveedores.find({});

        query.sort('-_id').exec((err, proveedores) => {

            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los proveedores'
                });
            }

            if (!proveedores) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay proveedores para mostrar '
                });
            }

            return res.status(200).send({
                status: 'success',
                proveedores
            });
        });
    },
    delete: (req, res) => {
        var proveedorId = req.params.id;
        Proveedores.findOneAndDelete({ _id: proveedorId }, (err, proveedorRemoved) => {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar'
                });
            }

            if (!proveedorRemoved) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado el proveedor, probablemente no exista.'
                });
            }

            return res.status(200).send({
                status: 'success',
                article: proveedorRemoved
            });
        });

    }
}

module.exports = controller;
