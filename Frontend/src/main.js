import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import VueRouter from 'vue-router'; 

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import Articulos from './components/Articulos.vue'
import CreateArticulo from './components/CreateArticulo.vue'
import EditArticulo from './components/EditArticulo.vue'
import Proveedores from './components/Proveedores.vue'
import CrearProveedores from './components/CrearProveedores.vue'
import Almacenes from './components/Almacenes.vue'
import CrearAlmacenes from './components/CrearAlmacenes.vue'
import Salidas from './components/Salidas.vue'
import CrearSalidas from './components/CrearSalidas.vue'
import MisSalidas from './components/MisSalidas.vue'
import Ordenes from './components/Ordenes.vue'
import CrearOrdenes from './components/CrearOrdenes.vue'
import MisOrdenes from './components/MisOrdenes.vue'
import Recepcion from './components/Recepcion.vue'
import CrearRecepcion from './components/CrearRecepcion.vue'

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(VueRouter);


const routes = [
  {path: '/articulos', component: Articulos},
  {path: '/crearArticles', component: CreateArticulo},
  {path: '/editArticle/:id', component: EditArticulo},
  {path: '/proveedores', component: Proveedores},
  {path: '/almacenes', component: Almacenes},
  {path: '/crearproveedor', component: CrearProveedores},
  {path: '/crearalmacen', component: CrearAlmacenes},
  {path: '/salidas', component: Salidas},
  {path: '/missalidas', component: MisSalidas},
  {path: '/crearsalida/:id', component: CrearSalidas},
  {path: '/ordenes', component: Ordenes},
  {path: '/misordenes', component: MisOrdenes},
  {path: '/crearorden/:id', component: CrearOrdenes},
  {path: '/recepciones', component: Recepcion},
  {path: '/crear-recepcion', component: CrearRecepcion},
  {path: '/', component: Articulos},
  {path: '*', component: Articulos},
];

const router = new VueRouter({
  routes, 
  mode: 'history'
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
