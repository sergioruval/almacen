class Articulo{
    constructor(nombre, precio, topeMinimo, topeMaximo){
        this.nombre = nombre;
        this.precio = precio;
        this.topeMinimo = topeMinimo;
        this.topeMaximo = topeMaximo; 
    }
}

export default Articulo;